#include <CommonLib.h>

void commonSetup(uint8_t numModules, Module *const modules[]) {
#ifdef USE_SERIAL
	Serial.begin(SERIAL_BAUD);
	while (!Serial) yield();
#endif

	SLOG("Beginning boot");

	/* Initialize display */
	displaySetup();

#ifndef NO_DISPLAY_MESSAGES
	/* Print welcome message while initializing */
	putLineF(0, "Initializing...");
#endif

	/* Initialize modules */
	for (uint8_t m = 0; m < numModules; m++) {
		Module *module = modules[m];

#ifndef NO_DISPLAY_MESSAGES
		putLineF(1, "Module %d/%d: %s", m + 1, numModules, module->getName());
#endif
		module->setup();

		if (didError()) {
			// Halt execution if module setup threw an error
			putLineF(OLED_ROWS - 1, "Error");
			return;
		}
	}

#ifndef NO_DISPLAY_MESSAGES
	/* Finally indicate that we're initialized */
	clearScreen();
	putLineF(OLED_ROWS - 1, "Ready");
#endif

	setInitialized(true);
}

void commonLoop(uint8_t numModules, Module *const modules[]) {
	/* Update display */
	displayLoop();

	if (!isInitialized() || didError()) {
		yield();
		return;
	}

#ifdef USE_DISPLAY_BATCHING
	displayStartBatch();
#endif

	/* Update modules */
	for (uint8_t m = 0; m < numModules; m++) {
		modules[m]->loop();
	}

#ifdef USE_DISPLAY_BATCHING
	displayEndBatch();
#endif
}
