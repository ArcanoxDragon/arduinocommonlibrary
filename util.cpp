#include <Arduino.h>
#include <stdio.h>

#include "util.h"
#include "display.h"

bool initialized = false;
bool errorThrown = false;

void error(const char *message) {
#ifdef USE_SERIAL
	char errMessage[strlen(message) + 8];
	snprintf(errMessage, sizeof(errMessage), "Error: %s", message);
#endif
#ifdef USE_DISPLAY
	clearScreen();
	putLine(6, F("ERROR:"));
	putLine(7, message);
#endif
	errorThrown = true;
}

void error(const __FlashStringHelper *message) {
	// read the flash memory into a buffer
	const char *messagePtr = reinterpret_cast<const char *>(message);
	uint32_t messageLen = strlen_P(messagePtr);
	char messageBuf[messageLen + 1];

	for (uint32_t i = 0; i < messageLen; i++) {
		messageBuf[i] = pgm_read_byte(messagePtr + i);
	}

	messageBuf[messageLen] = 0;

	error(messageBuf);
}

bool didError() {
	return errorThrown;
}

bool isInitialized() {
	return initialized;
}

void setInitialized(bool i) {
	initialized = i;
}

#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char *sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

int getFreeMemory() {
	char top;
#ifdef __arm__
	return &top - reinterpret_cast<char *>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
	return &top - __brkval;
#else  // __arm__
	return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}

#ifdef ARDUINO_ARCH_AVR

double getTimeScale() {
	uint8_t timer0PrescaleValue = TCCR0B & 0b111;
	double scaleDenominator;

	switch (timer0PrescaleValue) {
		case 0b001:
			scaleDenominator = 1;
			break;
		case 0b010:
			scaleDenominator = 8;
			break;
		case 0b100:
			scaleDenominator = 256;
			break;
		case 0b101:
			scaleDenominator = 1024;
			break;
		case 0b011:
		default:
			scaleDenominator = 64;
			break;
	}

	return 64 / scaleDenominator;
}

void delayScaled(uint64_t millis) {
	double timeScale = getTimeScale();
	uint64_t scaledMillis = (uint64_t) (timeScale * millis);

	delay(scaledMillis);
}

void delayScaledMicros(uint64_t micros) {
	double timeScale = getTimeScale();
	uint64_t scaledMicros = (uint64_t) (timeScale * micros);

	delayMicroseconds(scaledMicros);
}

#endif // #ifdef ARDUINO_ARCH_AVR
