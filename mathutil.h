#pragma once

#include <algorithm>
#include <cstdint>
#include <limits>
#include <type_traits>

template <typename T>
using no_infer = typename std::common_type<T>::type;

#define clamp(val, lowerBound, upperBound) std::min(std::max(val, lowerBound), upperBound)

#define feq(a, b) (fabs(a - b) < std::numeric_limits<decltype(a)>::epsilon())

#define smod(value, divisor) \
	(((value) >= 0) ? ((value) % (divisor)) : ((divisor) - (((divisor) - (value)) % (divisor))))

#define wrapInRange(value, range) \
	while (value >= range) {      \
		value -= range;           \
	}                             \
	while (value < 0) {           \
		value += range;           \
	}

inline double sfmod(double value, double divisor) {
	if (value >= 0) {
		return fmod(value, divisor);
	} else {
		return divisor - fmod(divisor - value, divisor);
	}
}

inline float sfmodf(float value, float divisor) {
	if (value >= 0) {
		return fmodf(value, divisor);
	} else {
		return divisor - fmodf(divisor - value, divisor);
	}
}

template <typename T = int>
inline T sign(T value) {
	static_assert(std::is_arithmetic<T>::value, "template argument not a numeric type");

	if (value > 0) {
		return (T) 1;
	} else if (value < 0) {
		return (T) -1;
	}

	return (T) 0;
}

template <typename T = double>
inline T linearScale(
	T			value,
	no_infer<T> fromMin,
	no_infer<T> fromMax,
	no_infer<T> toMin,
	no_infer<T> toMax,
	bool		clampOutput = true) {
	static_assert(std::is_floating_point<T>::value, "template argument not a floating-point type");

	T normalized = (value - fromMin) / (fromMax - fromMin);
	T result = normalized * (toMax - toMin) + toMin;

	return clampOutput ? clamp(result, toMin, toMax) : result;
}
