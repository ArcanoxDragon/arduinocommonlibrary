#pragma once

#if defined(USE_DISPLAY) || defined(_DISPLAY_CPP)

#include <stdint.h>
#include <WString.h>

#include "status_types.h"

void displaySetup();
void displayLoop();
void displayStartBatch();
void displayEndBatch();
void clearScreen();
void putLine(linepos_t line, const char *format, ...) __attribute__((format(printf, 2, 3)));
void putLine(linepos_t line, const __FlashStringHelper *format, ...);

#else

// Dummy defines

#define displaySetup()
#define displayLoop()
#define displayStartBatch()
#define displayEndBatch()
#define clearScreen()
#define putLine(line, format, ...)

#endif

#define putLineF(line, format, ...) putLine(line, F(format), ## __VA_ARGS__)
