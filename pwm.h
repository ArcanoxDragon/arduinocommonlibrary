#pragma once

#ifdef ARDUINO_ARCH_AVR

#include <stdint.h>

#define haltAndSyncTimers()		GTCCR = _BV(TSM) | _BV(PSRSYNC)
#define resumeTimers()			GTCCR = 0

#if defined(__AVR_ATmega2560__)
#define resetTimerCounts() \
		TCNT0 = 0; \
		TCNT1 = 0; \
		TCNT2 = 0; \
		TCNT3 = 0; \
		TCNT4 = 0
#else
#define resetTimerCounts() \
		TCNT0 = 0; \
		TCNT1 = 0; \
		TCNT2 = 0
#endif

typedef enum PwmFrequency {
	Pwm31372_Hz,
	Pwm3921_Hz,
	Pwm980_Hz,
	Pwm490_Hz,
	Pwm245_Hz,
	Pwm122_Hz,
	Pwm30_Hz,
} PwmFrequency;

typedef enum PwmFrequencyFast {
	FastPwm62500_Hz,
	FastPwm7812_Hz,
	FastPwm976_Hz,
	FastPwm244_Hz,
	FastPwm61_Hz,
	FastPwm30_Hz,
} PwmFrequencyFast;

/**
 * Sets the PWM frequency for pins on timers OTHER than Timer 0.
 */
void setPwmFrequency(uint8_t pin, PwmFrequency frequency);

/**
 * Sets the PWM frequency for pins on Timer 0.
 */
void setFastPwmFrequency(uint8_t pin, PwmFrequencyFast frequency);

#endif // #ifdef ARDUINO_ARCH_AVR
