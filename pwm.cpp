#include <Arduino.h>

#include "pwm.h"

#ifdef ARDUINO_ARCH_AVR

#define setTimerBits(reg, bits) ( (reg) = ((reg) & 0b11111000) | ((bits) & 0b111) )

#if defined(__AVR_ATmega2560__)

// TODO: Implement Mega

void setPwmFrequency(uint8_t pin, PwmFrequency frequency) {
	uint8_t timerBits;

	switch (pin) {

	}
}

void setFastPwmFrequency(uint8_t pin, PwmFrequencyFast frequency) {
	uint8_t timerBits;

	switch (pin) {

	}
}

#else

void setPwmFrequency(uint8_t pin, PwmFrequency frequency) {
	uint8_t timerBits;

	switch (pin) {
		case 9:
		case 10:
			switch (frequency) {
				case PwmFrequency::Pwm31372_Hz:
					timerBits = 0b00000001;
					break;
				case PwmFrequency::Pwm3921_Hz:
					timerBits = 0b00000010;
					break;
				case PwmFrequency::Pwm490_Hz:
					timerBits = 0b00000011;
					break;
				case PwmFrequency::Pwm122_Hz:
					timerBits = 0b00000100;
					break;
				case PwmFrequency::Pwm30_Hz:
					timerBits = 0b00000101;
					break;
				default:
					return;
			}

			setTimerBits(TCCR1B, timerBits);
			break;
		case 3:
		case 11:
			switch (frequency) {
				case PwmFrequency::Pwm31372_Hz:
					timerBits = 0b00000001;
					break;
				case PwmFrequency::Pwm3921_Hz:
					timerBits = 0b00000010;
					break;
				case PwmFrequency::Pwm980_Hz:
					timerBits = 0b00000011;
					break;
				case PwmFrequency::Pwm490_Hz:
					timerBits = 0b00000100;
					break;
				case PwmFrequency::Pwm245_Hz:
					timerBits = 0b00000101;
					break;
				case PwmFrequency::Pwm122_Hz:
					timerBits = 0b00000110;
					break;
				case PwmFrequency::Pwm30_Hz:
					timerBits = 0b00000111;
					break;
				default:
					return;
			}

			setTimerBits(TCCR2B, timerBits);
			break;
	}
}

void setFastPwmFrequency(uint8_t pin, PwmFrequencyFast frequency) {
	uint8_t timerBits;

	switch (pin) {
		case 5:
		case 6:
			switch (frequency) {
				case PwmFrequencyFast::FastPwm62500_Hz:
					timerBits = 0b00000001;
					break;
				case PwmFrequencyFast::FastPwm7812_Hz:
					timerBits = 0b00000010;
					break;
				case PwmFrequencyFast::FastPwm976_Hz:
					timerBits = 0b00000011;
					break;
				case PwmFrequencyFast::FastPwm244_Hz:
					timerBits = 0b00000100;
					break;
				case PwmFrequencyFast::FastPwm61_Hz:
					timerBits = 0b00000101;
					break;
				default:
					return;
			}

			setTimerBits(TCCR0B, timerBits);
			break;
	}
}

#endif // #if defined(__AVR_ATmega2560__)

#endif // #ifdef ARDUINO_ARCH_AVR
