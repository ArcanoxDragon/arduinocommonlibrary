#include "PidController.h"

#include "../mathutil.h"

#include <stdlib.h>

#ifdef ARDUINO_ARCH_AVR
#include <math.h>
#else
#include <cmath>
#endif

/* #region Public */

PidController::PidController(float pGain, float iGain, float dGain)
	: kP(pGain)
	, kI(iGain)
	, kD(dGain) {
	setProportionalExponent(1.0f);
	setIntegralExponent(1.0f);
	setDerivativeFilterWindowSize(1);
}

float PidController::get(float target, float actual, float dt) {
	// Calculate early values
	this->error = (target - actual) * this->scale;

	/* #region Proportional */
	float proportionalError;

	if (this->proportionalExponent == 1.0f) { // intentional exact float comparison
		proportionalError = this->error;
	} else if (this->proportionalExponent == 2.0f) { // intentional exact float comparison
		proportionalError = copysignf(this->error * this->error, this->error);
	} else {
		proportionalError = this->pExpLookup(this->error);
	}

	this->pOutput = this->kP * clamp(proportionalError, this->minError, this->maxError);

	/* #endregion Proportional */

	/* #region Derivative */

	this->delta = this->sampleDerivativeFilter(actual, dt);
	this->dOutput = this->kD * this->delta;

	/* #endregion Derivative  */

	/* #region Integral */

	// Apply I-lock and calculate integral
	this->iAutoLocked = this->iAutoLock && fabsf(this->delta) > this->iLockThreshold;
	this->isILocked = this->iLocked || this->iAutoLocked;

	if (this->iAutoLocked && this->iResetsOnLock) {
		this->integral = 0.0f;
	}

	if (!this->isILocked) {
		float integralDelta;

		if (this->integralExponent == 1.0f) { // intentional exact float comparison
			integralDelta = this->error * this->kI * dt;
		} else if (this->integralExponent == 2.0f) { // intentional exact float comparison
			integralDelta = copysignf(this->error * this->error, this->error) * this->kI * dt;
		} else {
			integralDelta = this->iExpLookup(this->error) * this->kI * dt;
		}

		this->integral = clamp(this->integral + integralDelta, this->minIntegral, this->maxIntegral);
	}

	this->iOutput = this->integral;

	/* #endregion Integral */

	this->output = clamp(this->pOutput + this->iOutput - this->dOutput, this->minOutput, this->maxOutput);
	this->lastValue = actual;
	this->lastTarget = target;
	this->lastError = this->error;

	return this->output;
}

float PidController::peek() {
	return this->output;
}

void PidController::reset(bool soft) {
	if (soft) {
		this->integral -= this->integral * this->kI;
	} else {
		this->integral = 0;
	}

	this->dFilterSum = 0.0f;
	this->dFilterCursor = 0;

	for (uint8_t i = 0; i < this->dFilterWindowSize; i++) {
		this->dFilterWindow[i] = 0.0f;
	}
}

void PidController::reset(float integralValue) {
	this->integral = clamp(integralValue, this->minIntegral, this->maxIntegral);
}

void PidController::setProportionalExponent(float proportionalExponent) {
	this->proportionalExponent = proportionalExponent;

	for (int32_t i = 0; i < EXPONENT_LUT_VALUE_COUNT; i++) {
		float x = i / (float) EXPONENT_LUT_VALUE_COUNT;

		this->pExpLut[i] = powf(x, proportionalExponent);
	}
}

void PidController::setIntegralExponent(float integralExponent) {
	this->integralExponent = integralExponent;

	for (int32_t i = 0; i < EXPONENT_LUT_VALUE_COUNT; i++) {
		float x = i / (float) EXPONENT_LUT_VALUE_COUNT;

		this->iExpLut[i] = powf(x, integralExponent);
	}
}

void PidController::setDerivativeFilterWindowSize(uint8_t windowSize) {
	if (windowSize < 1 || windowSize > MAX_DERIVATIVE_FILTER_WINDOW_SIZE) {
		abort();
		return;
	}

	this->dFilterWindowSize = windowSize;
	this->invDFilterWindowSize = 1.f / (float) windowSize;
	this->dFilterSum = 0.0f;
	this->dFilterCursor = 0;

	for (uint8_t i = 0; i < MAX_DERIVATIVE_FILTER_WINDOW_SIZE; i++) {
		this->dFilterWindow[i] = 0.f;
	}
}

float PidController::getP() {
	return this->pOutput;
}
float PidController::getI() {
	return this->iOutput;
}
float PidController::getD() {
	return this->dOutput;
}

/* #endregion Public */

/* #region Private */

float PidController::pExpLookup(float x) {
	if (x < -1 || x > 1) {
		return x;
	}

	int32_t i = (int32_t) (fabsf(x) * EXPONENT_LUT_VALUE_COUNT);

	return copysignf(this->pExpLut[i], x);
}

float PidController::iExpLookup(float x) {
	if (x < -1 || x > 1) {
		return x;
	}

	int32_t i = (int32_t) (fabsf(x) * EXPONENT_LUT_VALUE_COUNT);

	return copysignf(this->iExpLut[i], x);
}

float PidController::sampleDerivativeFilter(float actual, float dt) {
	float derivative = (actual - this->lastValue) * this->scale / dt;

	if (this->dFilterWindowSize == 1) {
		return derivative;
	}

	this->dFilterSum -= this->dFilterWindow[this->dFilterCursor];
	this->dFilterSum += derivative;
	this->dFilterWindow[this->dFilterCursor] = derivative;
	this->dFilterCursor = (this->dFilterCursor + 1) % this->dFilterWindowSize;

	return this->dFilterSum * this->invDFilterWindowSize;
}

/* #endregion Private */
