#pragma once

#include <cstdint>
#include <limits>

class PidController {
public:
	static const uint8_t MAX_DERIVATIVE_FILTER_WINDOW_SIZE = 32;

	float kP, kI, kD;
	float minError = -std::numeric_limits<float>::max();
	float maxError = std::numeric_limits<float>::max();
	float minOutput = -1.0f;
	float maxOutput = 1.0f;
	float minIntegral = -1.0f;
	float maxIntegral = 1.0f;
	float scale = 1.0f;
	float iLockThreshold = 0.0f;
	bool  iLocked = false;
	bool  iAutoLock = false;
	bool  iResetsOnLock = false;

	PidController(float pGain = 0.0f, float iGain = 0.0f, float dGain = 0.0f);

	float get(float target, float actual, float dt);
	float peek();
	void  reset(bool soft = false);
	void  reset(float integralValue);

	void setProportionalExponent(float proportionalExponent);
	void setIntegralExponent(float integralExponent);
	void setDerivativeFilterWindowSize(uint8_t windowSize);

	float getP();
	float getI();
	float getD();

private:
	static const int EXPONENT_LUT_VALUE_COUNT = 100;

	bool	iAutoLocked = false;
	bool	isILocked = false;
	float	error = 0.0f;
	float	delta = 0.0f;
	float	lastValue = 0.0f;
	float	lastTarget = 0.0f;
	float	lastError = 0.0f;
	float	integral = 0.0f;
	float	pOutput = 0.0f;
	float	iOutput = 0.0f;
	float	dOutput = 0.0f;
	float	output = 0.0f;
	float	proportionalExponent = 1.0f;
	float	integralExponent = 1.0f;
	uint8_t dFilterWindowSize = 1;
	float	invDFilterWindowSize = 1.0f;
	float	dFilterSum = 0.0f;
	uint8_t dFilterCursor = 0;

	float pExpLut[EXPONENT_LUT_VALUE_COUNT];
	float iExpLut[EXPONENT_LUT_VALUE_COUNT];
	float dFilterWindow[MAX_DERIVATIVE_FILTER_WINDOW_SIZE];

	float pExpLookup(float x);
	float iExpLookup(float x);
	float sampleDerivativeFilter(float actual, float dt);
};
