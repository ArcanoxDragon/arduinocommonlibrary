#pragma once

#include "oled.h"
#include "status_types.h"
#include "util.h"

class StatusManager {
	time_t lastScroll = 0;
	linelen_t messagePos[OLED_ROWS];
	linelen_t messageLen[OLED_ROWS];
	char *messages[OLED_ROWS];
	OledDisplay *oled = NULL;
	bool inBatch = false;
	bool batchDidUpdate = false;

	void formatLine(linepos_t line, const char *message);
	void scroll();
	void clearLine(linepos_t line);
	void write(linepos_t line);

   public:
	StatusManager(OledDisplay *lcd);
	~StatusManager();

	void update();
	void clear();
	void put(linepos_t line, const char* message);
	void resetLineScroll(linepos_t line);
	void startBatch();
	void endBatch();
};
