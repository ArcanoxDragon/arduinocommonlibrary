#ifdef USE_DISPLAY

#include "util.h"

#include <Arduino.h>
#include <assert.h>

#include "StatusManager.h"

#define assert_line(line) assert(line < OLED_ROWS)

// #region Public

StatusManager::StatusManager(OledDisplay *oled) {
	this->oled = oled;

	for (linepos_t line = 0; line < OLED_ROWS; line++) {
		this->messages[line] = (char *) malloc((1 + LINE_BUFFER_SIZE) * sizeof(char));

		if (this->messages[line] <= 0) {
#ifdef USE_SERIAL
			Serial.print(F("Failed to allocate memory for StatusManager (at line "));
			Serial.print(line);
			Serial.println(F(")"));

			int memFree = getFreeMemory();

			Serial.print(F("Free memory (between stack and edge of heap): "));
			Serial.print(memFree);
			Serial.println(F(" bytes"));
			Serial.flush();
#endif
			return;
		}
	}

	this->clear();
}

StatusManager::~StatusManager() {
	for (linepos_t line = 0; line < OLED_ROWS; line++) {
		free(this->messages[line]);
		this->messages[line] = NULL;
	}
}

void StatusManager::update() {
	if (!this->messages) return;

	if (millis() - this->lastScroll >= OLED_SCROLL_INTERVAL) {
		this->scroll();
	}
}

void StatusManager::clear() {
	if (!this->messages) return;

	this->oled->clearDisplay();

	for (linepos_t line = 0; line < OLED_ROWS; line++) {
		this->put(line, "");
	}
}

void StatusManager::put(linepos_t line, const char *message) {
	linelen_t messageLen = min(strlen(message), LINE_BUFFER_SIZE);

	if (messageLen > OLED_COLS) {
		// Will scroll

		linelen_t actualMessageLen = min(LINE_BUFFER_SIZE, messageLen + OLED_LINE_WRAP_SPACES);

		this->messagePos[line] = min(this->messagePos[line], actualMessageLen);
	} else {
		// Will not scroll

		this->messagePos[line] = 0;
	}

	this->messageLen[line] = messageLen;
	this->formatLine(line, message);
	this->write(line);

	if (this->inBatch)
		this->batchDidUpdate = true;
	else
		this->oled->display();
}

void StatusManager::resetLineScroll(linepos_t line) {
	assert_line(line);

	this->messagePos[line] = 0;
}

void StatusManager::startBatch() {
	this->inBatch = true;
}

void StatusManager::endBatch() {
	if (this->batchDidUpdate)
		this->oled->display();

	this->inBatch = false;
	this->batchDidUpdate = false;
}

// #endregion Public

// #region Private

void StatusManager::formatLine(linepos_t line, const char *message) {
	if (!this->messages) return;

	linelen_t messageLen = this->messageLen[line];
	char *dst = this->messages[line];

	memcpy(dst, message, messageLen); // messageLen will always be <= LINE_BUFFER_SIZE

	if (messageLen > OLED_COLS) {
		// Pad OLED_LINE_WRAP_SPACES space characters onto the end of the message so that
		// when it wraps, there is space between the start and end of the text

		uint8_t numSpaces = min(OLED_LINE_WRAP_SPACES, LINE_BUFFER_SIZE - messageLen); // domain is [0, OLED_LINE_WRAP_SPACES]

		memset(dst + messageLen, ' ', numSpaces);

		messageLen += numSpaces;
		this->messageLen[line] = messageLen;
	}

	// Put a line terminator at the end
	dst[messageLen] = '\0';
}

void StatusManager::scroll() {
	if (!this->messages) return;

	bool shouldRefresh = false;

	for (linepos_t line = 0; line < OLED_ROWS; line++) {
		if (this->messageLen[line] <= OLED_COLS)
			continue;

		shouldRefresh = true;
		this->messagePos[line]++;
		this->messagePos[line] %= this->messageLen[line];
		this->write(line);
	}

	if (shouldRefresh) {
		this->oled->display();
	}

	this->lastScroll = millis();
}

void StatusManager::clearLine(linepos_t line) {
	assert_line(line);

	uint16_t lineTop = OLED_CHAR_HEIGHT * line;

	// Clear the line and the print the text
	this->oled->fillRect(0, lineTop, OLED_WIDTH, OLED_CHAR_HEIGHT, BLACK);
	this->oled->setCursor(0, lineTop);
}

void StatusManager::write(linepos_t line) {
	if (!this->messages) return;

	assert_line(line);

	this->clearLine(line);

	if (this->messagePos[line] == 0) {
		// Straight print

		this->oled->write(this->messages[line]);
	} else {
		linelen_t len = this->messageLen[line];
		linelen_t offset = this->messagePos[line];
		linelen_t remainder = len - offset;
		char buffer[len + 1];

		// Copy the offset line in two steps
		memcpy(buffer, this->messages[line] + offset, remainder);
		memcpy(buffer + remainder, this->messages[line], offset);

		buffer[len] = '\0';
		this->oled->print(buffer);
	}
}

// #endregion Private

#endif
