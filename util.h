#pragma once

#include <WString.h>

#ifndef SERIAL_BAUD
#define SERIAL_BAUD 115200
#endif

#ifdef USE_SERIAL
#define SLOG(msg) Serial.println(F(msg)); \
                  Serial.flush()
#else
#define SLOG(msg)
#endif

void error(const char *message);
void error(const __FlashStringHelper *message);
bool didError();
bool isInitialized();
void setInitialized(bool initialized);

int getFreeMemory();

#define ARRAYLEN(array) sizeof(array) / sizeof(array[0])

#ifdef ARDUINO_ARCH_AVR

typedef uint32_t time_t;

double getTimeScale();
void delayScaled(time_t millis);
void delayScaledMicros(time_t micros);

#endif // #ifdef ARDUINO_ARCH_AVR
