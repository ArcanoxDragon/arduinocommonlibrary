#include "oled.h"

#ifndef LINE_BUFFER_SIZE
#define LINE_BUFFER_SIZE 64
#endif

// Conditional line-length data structures depending on space needed
#if LINE_BUFFER_SIZE < 256
typedef uint8_t linelen_t;
#elif LINE_BUFFER_SIZE < 65536
typedef uint16_t linelen_t;
#else
typedef uint32_t linelen_t;
#endif

// Conditional line-position data structures depending on space needed
#if OLED_ROWS < 256
typedef uint8_t linepos_t;
#elif OLED_ROWS < 65536
typedef uint16_t linepos_t;
#else
typedef uint32_t linepos_t;
#endif
